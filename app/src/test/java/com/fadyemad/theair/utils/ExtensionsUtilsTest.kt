package com.fadyemad.theair.utils

import com.fadyemad.theair.models.Genere
import junit.framework.TestCase

class ExtensionsUtilsTest : TestCase() {

    fun testToCommaSeparatedString_null_returnsNull() {
        val generes: List<Genere>? = null
        val funRes = generes?.toCommaSeparatedString()

        assertEquals(null, funRes)
    }

    fun testToCommaSeparatedString_emptyData_returnsEmpty() {
        val generes: List<Genere> = listOf()
        val funRes = generes.toCommaSeparatedString()
        val expectedRes = String()

        assertEquals(expectedRes, funRes)
    }

    fun testToCommaSeparatedString_oneElement_returnsElementWithoutComma() {
        val genereAnim = "Anim"
        val generes: List<Genere> = listOf(Genere(genereAnim))
        val funRes = generes.toCommaSeparatedString()

        assertEquals(genereAnim, funRes)
    }


    fun testToCommaSeparatedString_twoElements_returnsTheElementsWithComma() {
        val genereAnim = "Anim"
        val genereComedy = "Comedy"
        val generes: List<Genere> = listOf(
            Genere(genereAnim),
            Genere(genereComedy),
        )
        val funRes = generes.toCommaSeparatedString()
        val expectedRes = "$genereAnim, $genereComedy"

        assertEquals(expectedRes, funRes)
    }

    fun testToCommaSeparatedString_moreElements_returnsTheElementsWithComma() {
        val genereAnim = "Anim"
        val genereComedy = "Comedy"
        val genereLoveStory = "Love Story"
        val genereActionAndAdventure = "Action & Adventure"
        val generes: List<Genere> = listOf(
            Genere(genereAnim),
            Genere(genereComedy),
            Genere(genereLoveStory),
            Genere(genereActionAndAdventure)
        )
        val funRes = generes.toCommaSeparatedString()
        val expectedRes = "$genereAnim, $genereComedy, $genereLoveStory, $genereActionAndAdventure"

        assertEquals(expectedRes, funRes)
    }

}