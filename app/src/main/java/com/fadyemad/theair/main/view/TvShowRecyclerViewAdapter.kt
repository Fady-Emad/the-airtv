package com.fadyemad.theair.main.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fadyemad.theair.databinding.TvShowItemLayoutBinding
import com.fadyemad.theair.models.Show


class TvShowRecyclerViewAdapter(val onItemClickListener: OnItemClickListener) :
    ListAdapter<Show, TvShowRecyclerViewAdapter.TvShowViewHolder>(ShowDiffUtil()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        return TvShowViewHolder.from(parent)
    }

    override fun onBindViewHolder(holderTv: TvShowViewHolder, position: Int) {
        holderTv.bind(getItem(position), onItemClickListener)
    }

    class TvShowViewHolder private constructor(val binding: TvShowItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): TvShowViewHolder {
                val view = TvShowItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent, false)
                return TvShowViewHolder(view)
            }
        }

        fun bind(Show: Show, onItemClickListener: OnItemClickListener) {
            binding.show = Show
            binding.clickListener = onItemClickListener

            binding.executePendingBindings()
        }

    }


    class ShowDiffUtil : DiffUtil.ItemCallback<Show>() {
        override fun areItemsTheSame(oldItem: Show, newItem: Show) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Show, newItem: Show) =
            oldItem.equals(newItem)
    }

    class OnItemClickListener (val clickListener: (Show: Show) -> Unit){
        fun onItemClick (Show: Show) =
            clickListener(Show)
    }
}
