package com.fadyemad.theair.main.view

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.fadyemad.theair.R
import com.fadyemad.theair.databinding.FragmentHomeBinding
import com.fadyemad.theair.main.viewmodel.HomeViewModel


class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: HomeViewModel
    lateinit var menu: Menu
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get<HomeViewModel>()
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tvShowRecyclerViewAdapter =
            TvShowRecyclerViewAdapter(TvShowRecyclerViewAdapter.OnItemClickListener {
                findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragment2ToDetailsFragment(
                        it
                    )
                )
            })
        binding.rvShows.apply {
            adapter = tvShowRecyclerViewAdapter
            layoutManager = GridLayoutManager(this.context, 2)
        }

        viewModel.shows.observe(viewLifecycleOwner) {shows ->
            tvShowRecyclerViewAdapter.submitList(shows)
        }


        viewModel.loadingStates.observe(viewLifecycleOwner){
            when(it){
                HomeViewModel.LoadingStates.LOADING -> {
                    binding.pbLoading.visibility = View.VISIBLE
                    binding.tvErrorMessage.visibility = View.GONE
                }
                HomeViewModel.LoadingStates.SUCCESS_WITH_DATA -> {
                    binding.pbLoading.visibility = View.GONE
                    binding.tvErrorMessage.visibility = View.GONE
                    binding.rvShows.visibility = View.VISIBLE
                }
                HomeViewModel.LoadingStates.SUCCESS_WITHOUT_DATA ->{
                    binding.pbLoading.visibility = View.GONE
                    binding.rvShows.visibility = View.GONE
                    binding.tvErrorMessage.visibility = View.VISIBLE
                    binding.tvErrorMessage.text = getString(R.string.str_no_item_found)
                }
                HomeViewModel.LoadingStates.ERROR_UNKNOWN_HOST -> {
                    binding.pbLoading.visibility = View.GONE
                    binding.rvShows.visibility = View.GONE
                    binding.tvErrorMessage.visibility = View.VISIBLE
                    binding.tvErrorMessage.text = getString(R.string.str_check_your_internet_connection)
                }
                HomeViewModel.LoadingStates.ERROR_GENERAL_EXCEPTION ->{
                    binding.pbLoading.visibility = View.GONE
                    binding.rvShows.visibility = View.GONE
                    binding.tvErrorMessage.visibility = View.VISIBLE
                    binding.tvErrorMessage.text = getString(R.string.str_general_error)
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_menu, menu)
        this.menu = menu
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionFavourites -> {
                item.isVisible = false
                viewModel.getTvShowsOfType(HomeViewModel.TvShowsTypes.FAVOURITES)
                menu.findItem(R.id.actionTopRated).isVisible = true
            }
            R.id.actionTopRated -> {
                item.isVisible = false
                viewModel.getTvShowsOfType(HomeViewModel.TvShowsTypes.TOP_RATED)
                menu.findItem(R.id.actionFavourites).isVisible = true
            }
        }

        return super.onOptionsItemSelected(item)
    }

}