package com.fadyemad.theair.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fadyemad.theair.models.Show
import com.fadyemad.theair.network.Backend
import com.fadyemad.theair.network.responses.TvShowsApiResponse
import kotlinx.coroutines.*
import java.lang.Exception
import java.net.UnknownHostException

class HomeViewModel : ViewModel() {

    private var uiJob = Job()
    private val coroutineScope = CoroutineScope(uiJob + Dispatchers.Main)

    private val _shows = MutableLiveData<List<Show>>()
    val shows: LiveData<List<Show>>
        get() = _shows

    private val _loadingStates = MutableLiveData<LoadingStates>()
    val loadingStates: LiveData<LoadingStates>
        get() = _loadingStates

    init {
        getTvShowsOfType(TvShowsTypes.TOP_RATED)
    }


    fun getTvShowsOfType(tvShowsTypes: TvShowsTypes) {
        coroutineScope.launch {
            try {
                _loadingStates.value = LoadingStates.LOADING
                val tvShowsApiResponse: TvShowsApiResponse
                when (tvShowsTypes) {
                    TvShowsTypes.FAVOURITES -> {
                        tvShowsApiResponse = Backend.retrofitService.getFavourites().await()
                    }
                    else ->{
                        tvShowsApiResponse = Backend.retrofitService.getTopRated().await()
                        // sorting by vote
                        //tvShowsApiResponse.results.sortedByDescending { it.voteAverage }
                    }
                }
                if (tvShowsApiResponse.results.isNullOrEmpty()) {
                    _loadingStates.value = LoadingStates.SUCCESS_WITHOUT_DATA
                } else {
                    _loadingStates.value = LoadingStates.SUCCESS_WITH_DATA
                    _shows.value = tvShowsApiResponse.results
                }
            } catch (e: UnknownHostException) {
                _loadingStates.value = LoadingStates.ERROR_UNKNOWN_HOST
            } catch (e: Exception) {
                _loadingStates.value = LoadingStates.ERROR_GENERAL_EXCEPTION

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        uiJob.cancel()
    }

    enum class TvShowsTypes {
        TOP_RATED,
        FAVOURITES
    }

    enum class LoadingStates {
        LOADING,
        SUCCESS_WITH_DATA,
        SUCCESS_WITHOUT_DATA,
        ERROR_UNKNOWN_HOST,
        ERROR_GENERAL_EXCEPTION
    }
}