package com.fadyemad.theair

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fadyemad.theair.main.view.MainActivity
import com.fadyemad.theair.utils.startActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity<MainActivity>()
        finish()
    }

}