package com.fadyemad.theair.tvshowdetails.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import com.fadyemad.theair.R
import com.fadyemad.theair.databinding.FragmentTvShowDetailsBinding
import com.fadyemad.theair.main.view.TvShowRecyclerViewAdapter
import com.fadyemad.theair.tvshowdetails.viewmodel.TvShowDetailsViewModel
import com.fadyemad.theair.tvshowdetails.viewmodel.TvShowDetailsViewModelFactory
import com.google.android.material.snackbar.Snackbar
import com.fadyemad.theair.utils.onClick
import com.fadyemad.theair.utils.setFavourite

class TvShowDetailsFragment : Fragment() {

    lateinit var binding: FragmentTvShowDetailsBinding
    lateinit var viewModel: TvShowDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewModeFactory = TvShowDetailsViewModelFactory(
            TvShowDetailsFragmentArgs.fromBundle(
                requireArguments()
            ).show
        )
        viewModel = ViewModelProvider(this, viewModeFactory).get<TvShowDetailsViewModel>()

        binding = FragmentTvShowDetailsBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tvNetworkRecyclerViewAdapter = TvNetworkRecyclerViewAdapter()
        binding.rvNetworks.apply {
            adapter = tvNetworkRecyclerViewAdapter
            this.layoutManager =
                LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        }

        val castRecyclerViewAdapter = TvCastRecyclerViewAdapter()
        binding.rvCast.apply {
            adapter = castRecyclerViewAdapter
            this.layoutManager =
                LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        }

        val showRecyclerViewAdapter =
            TvShowRecyclerViewAdapter(TvShowRecyclerViewAdapter.OnItemClickListener {
                findNavController().navigate(
                    TvShowDetailsFragmentDirections.actionDetailsFragmentToDetailsFragment(
                        it
                    )
                )
            })
        binding.rvRecommended.apply {
            adapter = showRecyclerViewAdapter
            this.layoutManager = GridLayoutManager(this.context, 2)
        }

        viewModel.recommendationsAndSimilar.observe(viewLifecycleOwner) { shows ->
            binding.recommendationsAndSimilarVisibility = !shows.isNullOrEmpty()
            showRecyclerViewAdapter.submitList(shows)
        }

        viewModel.casts.observe(viewLifecycleOwner) { casts ->
            binding.castVisibility = !casts.isNullOrEmpty()
            castRecyclerViewAdapter.submitList(casts)
        }

        viewModel.tvNetworks.observe(viewLifecycleOwner) { tvNetworks ->
            binding.tvNetworkVisibility = !tvNetworks.isNullOrEmpty()
            tvNetworkRecyclerViewAdapter.submitList(tvNetworks)
        }


        /**
         * I intended to do the click listener like this
         * in order to show my skills in manipulating with extension function
         */
        binding.btnOriginalPage onClick {
            val homeUrl = viewModel.show.value?.homepageUrl
            if (!homeUrl.isNullOrEmpty()) {
                goToWeb(homeUrl)
            } else {
                Snackbar.make(
                    binding.root,
                    getString(R.string.str_does_not_has_a_home_page),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        viewModel.ratingStars.observe(viewLifecycleOwner) { rate ->
            binding.rbRating.rating = rate
            binding.rbRating.onRatingBarChangeListener =
                RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
                    viewModel.submitRating(rating)
                }
        }

        viewModel.afterSubmitRate.observe(viewLifecycleOwner) { rateResponse ->
            Snackbar.make(binding.root, rateResponse.message, Snackbar.LENGTH_SHORT).show()
        }

        viewModel.afterSubmitFavourite.observe(viewLifecycleOwner) { favouriteResponse ->
            Snackbar.make(binding.root, favouriteResponse.message, Snackbar.LENGTH_SHORT).show()
            binding.iBtnFavourite.setFavourite(favouriteResponse.isFavourite())
        }

    }

    private fun goToWeb(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }
}