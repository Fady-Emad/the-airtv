package com.fadyemad.theair.tvshowdetails.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fadyemad.theair.models.Show
import java.lang.IllegalArgumentException

class TvShowDetailsViewModelFactory (val Show: Show) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TvShowDetailsViewModel::class.java))
            return TvShowDetailsViewModel(Show) as T
        throw IllegalArgumentException("Unknow ViewModel ${modelClass.name}")
    }
}