package com.fadyemad.theair.tvshowdetails.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fadyemad.theair.models.*
import com.fadyemad.theair.network.requests.*
import com.fadyemad.theair.network.responses.SubmitFavouriteResponse
import com.fadyemad.theair.network.responses.SubmitRatingResponse
import com.fadyemad.theair.network.Backend
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception

class TvShowDetailsViewModel( private var showModel: Show) :
    ViewModel() {
    private var uiJob = Job()
    private val coroutineScope = CoroutineScope(uiJob + Dispatchers.Main)

    private val _show = MutableLiveData<Show>()
    val show: LiveData<Show>
        get() = _show

    private val _recommendationsAndSimilar = MutableLiveData<List<Show>>()
    val recommendationsAndSimilar: LiveData<List<Show>>
        get() = _recommendationsAndSimilar

    private val _tvNetworks = MutableLiveData<List<TVNetwork>>()
    val tvNetworks: LiveData<List<TVNetwork>>
        get() = _tvNetworks

    private val _casts = MutableLiveData<List<Cast>>()
    val casts: LiveData<List<Cast>>
        get() = _casts

    private val _ratingStars = MutableLiveData<Float>()
    val ratingStars: LiveData<Float>
        get() = _ratingStars

  private val _afterSubmitFavourite = MutableLiveData<SubmitFavouriteResponse>()
    val afterSubmitFavourite: LiveData<SubmitFavouriteResponse>
        get() = _afterSubmitFavourite

    private val _afterSubmitRate = MutableLiveData<SubmitRatingResponse>()
    val afterSubmitRate: LiveData<SubmitRatingResponse>
        get() = _afterSubmitRate

    init {
        _show.value = showModel
        getShowDetails()
    }

    private fun getShowDetails() {
        coroutineScope.launch {
            try {
                showModel = Backend.retrofitService.getShowDetails(showModel.id).await()
                _show.value = showModel
                _tvNetworks.value = showModel.networks
                showRecommendationAndSimilar(showModel.recommendations, showModel.similar)
                _casts.value = showModel.credits?.cast
                _ratingStars.value = showModel.accountStates?.rate?.value?: 0.0f
            } catch (e: Exception) {
                Log.e(this.javaClass.name, e.message)
                e.printStackTrace()
            }
        }

    }

    private fun showRecommendationAndSimilar(
        recommendations: TvShowRecommendations?,
        similar: TvShowRecommendations?
    ) {
        val recommended: MutableList<Show> = arrayListOf()
        if (recommendations != null) {
            if (!recommendations.results.isNullOrEmpty()) {
                recommended.addAll(recommendations.results)
            }
        }
        if (similar != null) {
            if (!similar.results.isNullOrEmpty()) {
                recommended.addAll(similar.results)
            }
        }
        _recommendationsAndSimilar.value = recommended
    }


    fun submitRating(rating: Float) {
        coroutineScope.launch {
            try {
                val tvRatingResponse =
                    Backend.retrofitService.submitRating(showModel.id, TvRateRequestBody(rating))
                        .await()
                _afterSubmitRate.value = tvRatingResponse
            } catch (e: Exception) {
                Log.e(this.javaClass.name, e.message)
                e.printStackTrace()
            }
        }
    }

    fun markAsFavourite(isFav: Boolean) {
        coroutineScope.launch {
            try {
                val submitFavouriteResponse =
                    Backend.retrofitService.submitFavourite(AddFavouriteRequestBody(showModel.id, !isFav))
                        .await()
                showModel.accountStates?.isFavorite = submitFavouriteResponse.isFavourite()
                _afterSubmitFavourite.value = submitFavouriteResponse
            } catch (e: Exception) {
                Log.e(this.javaClass.name, e.message)
                e.printStackTrace()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        uiJob.cancel()
    }

}