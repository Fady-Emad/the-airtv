package com.fadyemad.theair.tvshowdetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fadyemad.theair.databinding.TvNetworkItemLayoutBinding
import com.fadyemad.theair.models.TVNetwork


class TvNetworkRecyclerViewAdapter :
    ListAdapter<TVNetwork, TvNetworkRecyclerViewAdapter.TvNetworkViewHolder>(TVNetworkDiffUtil()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvNetworkViewHolder {
        return TvNetworkViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: TvNetworkViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class TvNetworkViewHolder private constructor(val binding: TvNetworkItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): TvNetworkViewHolder {
                val view = TvNetworkItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent, false)
                return TvNetworkViewHolder(view)
            }
        }

        fun bind(newItem: TVNetwork) {
            binding.tvNetwork = newItem
            binding.executePendingBindings()
        }

    }


    class TVNetworkDiffUtil : DiffUtil.ItemCallback<TVNetwork>() {
        override fun areItemsTheSame(oldItem: TVNetwork, newItem: TVNetwork) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: TVNetwork, newItem: TVNetwork) =
            oldItem.equals(newItem)
    }

}
