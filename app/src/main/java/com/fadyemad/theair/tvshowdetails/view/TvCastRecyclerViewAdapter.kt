package com.fadyemad.theair.tvshowdetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fadyemad.theair.databinding.TvCastItemLayoutBinding
import com.fadyemad.theair.models.Cast


class TvCastRecyclerViewAdapter :
    ListAdapter<Cast, TvCastRecyclerViewAdapter.CastViewHolder>(CastDiffUtil()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
        return CastViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CastViewHolder private constructor(val binding: TvCastItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): CastViewHolder {
                val view = TvCastItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent, false)
                return CastViewHolder(view)
            }
        }

        fun bind(cast: Cast) {
            binding.cast = cast
            binding.executePendingBindings()
        }

    }


    class CastDiffUtil : DiffUtil.ItemCallback<Cast>() {
        override fun areItemsTheSame(oldItem: Cast, newItem: Cast) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Cast, newItem: Cast) =
            oldItem.equals(newItem)
    }

}
