package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cast (
    @Json(name = "id")
    val id: Int,

    @Json(name = "name")
    val name: String?,

    @Json(name = "character")
    val character: String?,

    @Json(name = "profile_path")
    val profileUrl: String?,

): Parcelable