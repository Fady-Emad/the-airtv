package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountStates(
    @Json(name = "favorite")
    var isFavorite : Boolean?,

    @Json(name = "rated")
    val rate : TvRate
    ) : Parcelable