package com.fadyemad.theair.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvRate (
    val value : Float
): Parcelable