package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvShowRecommendations(
    @Json(name = "results")
    val results : List<Show> = listOf()
) : Parcelable