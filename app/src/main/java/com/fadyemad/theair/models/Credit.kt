package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
class Credit (
    @Json(name = "cast")
    val cast : List<Cast> = listOf()
) : Parcelable