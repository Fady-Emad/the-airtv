package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Show (
    @Json(name = "id")
    val id : Int,

    @Json(name = "name")
    val name : String,

    @Json(name = "first_air_date")
    val firstAirDate : String? = "",

    @Json(name = "poster_path")
    val posterUrl : String?,

    @Json(name = "vote_average")
    val voteAverage : Double?,

    @Json(name = "homepage")
    val homepageUrl : String?,

    @Json(name = "overview")
    val overview : String?,

    @Json(name = "number_of_episodes")
    val episodesNumbers : Int?,

    @Json(name = "genres")
    val geners : List<Genere>?,

    @Json(name = "networks")
    val networks : List<TVNetwork>?,

    @Json(name = "credits")
    val credits : Credit?,

    @Json(name = "recommendations")
    val recommendations : TvShowRecommendations?,

    @Json(name = "similar")
    val similar : TvShowRecommendations?,

    @Json(name = "account_states")
    val accountStates : AccountStates?

    ) : Parcelable