package com.fadyemad.theair.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Genere(
    @Json(name = "name")
    val name: String,
    @Json(name = "id")
    val id: Int = 0

) : Parcelable