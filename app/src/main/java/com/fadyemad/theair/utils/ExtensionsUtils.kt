package com.fadyemad.theair.utils

import com.fadyemad.theair.models.Genere

fun List<Genere>.toCommaSeparatedString() : String{
    var formate = String()
    this.forEach {
        formate += it.name + ", "
    }
    return formate.removeSuffix(", ")
}