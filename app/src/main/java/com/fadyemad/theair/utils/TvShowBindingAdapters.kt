package com.fadyemad.theair.utils

import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.fadyemad.theair.R
import com.fadyemad.theair.models.Genere

@BindingAdapter("setRating")
fun TextView.setRating(voteAverage: Double?) {
    val rate = voteAverage?.times(10)?.toInt()
    val data = "${rate ?: 0}%"
    this.text = data
}

@BindingAdapter("setImageUrl")
fun ImageView.setImageUrl(imgUrl: String?) {
    var fullImageUrl: String = ""
    if (imgUrl != null) {
        val prefix = "https://image.tmdb.org/t/p/original"
        fullImageUrl = if (imgUrl.startsWith(prefix)) imgUrl else prefix + imgUrl
    }
    Glide.with(this.context)
        .load(fullImageUrl)
        .placeholder(R.drawable.loading_spinner)
        .error(R.drawable.ic_broken_image)
        .into(this)

}

@BindingAdapter("setEpisodeNumbers")
fun TextView.setEpisodeNumbers(episodesNumbers: Int?) {
    if (episodesNumbers != null) {
        this.visibility = View.VISIBLE
        val formate = "${episodesNumbers} ${context.getString(R.string.str_episode)}"
        this.text = formate
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("setGeneres")
fun TextView.setGeneres(generes: List<Genere>?) {
    if (generes != null) {
        this.visibility = View.VISIBLE
        this.text = generes.toCommaSeparatedString()
    } else {
        this.visibility = View.GONE
    }
}
@BindingAdapter("setFavourite")
fun ImageButton.setFavourite(isFav: Boolean?) {
    if (isFav != null) {
        if (isFav){
            this.setImageResource(R.drawable.ic_favorite_filled_24)
        }else {
            this.setImageResource(R.drawable.ic_favorite_border_24)
        }
    } else {
        this.setImageResource(R.drawable.ic_favorite_border_24)
    }
}
