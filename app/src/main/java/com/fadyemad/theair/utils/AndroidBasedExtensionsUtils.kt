package com.fadyemad.theair.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View


inline fun <reified T : Activity> Activity.startActivity(options: Bundle? = null) =
    startActivityForResult<T>(-1, options)

inline fun <reified T : Activity> Activity.startActivityForResult(requestCode : Int, options: Bundle? = null) =
    startActivityForResult(Intent(this, T::class.java), requestCode, options)

infix fun View.onClick (onClickListener: (View) -> Unit) =
    setOnClickListener(onClickListener)
