package com.fadyemad.theair.network.responses

import com.squareup.moshi.Json

const val STATUS_CODE_MARKED_FAVOURITE = 1
const val STATUS_CODE_ALREADY_MARKED_FAVOURITE = 12

data class SubmitFavouriteResponse(
    @Json(name = "success")
    val success: Boolean,
    @Json(name = "status_code")
    val statusCode: Int,
    @Json(name = "status_message")
    val message: String
){
    fun isFavourite () = (statusCode == STATUS_CODE_MARKED_FAVOURITE || statusCode == STATUS_CODE_ALREADY_MARKED_FAVOURITE)
}