package com.fadyemad.theair.network.requests

import com.squareup.moshi.Json

data class AddFavouriteRequestBody (
    @Json(name = "media_id")
    val mediaId : Int ,
    @Json(name = "favorite")
    val favorite : Boolean = true,

    @Json(name = "media_type")
    val mediaType : String = "tv",

)