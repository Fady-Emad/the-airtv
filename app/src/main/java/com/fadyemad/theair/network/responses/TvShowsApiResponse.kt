package com.fadyemad.theair.network.responses

import com.fadyemad.theair.models.Show
import com.squareup.moshi.Json

data class TvShowsApiResponse (
    @Json(name = "page")
    val page : Int = 1,
    @Json(name = "total_results")
    val totalResults : Int = 0,
    @Json(name = "total_pages")
    val totalpages : Int = 1,
    @Json(name = "results")
    val results : List<Show>

)