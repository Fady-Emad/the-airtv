package com.fadyemad.theair.network

import com.fadyemad.theair.models.Show
import com.fadyemad.theair.network.requests.*
import com.fadyemad.theair.network.responses.SubmitFavouriteResponse
import com.fadyemad.theair.network.responses.SubmitRatingResponse
import com.fadyemad.theair.network.responses.TvShowsApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.*

private const val TOKEN = "5f75a8f9fca385a41ee23869c7ef6d5c3cafffc9"
private const val API_KEY = "23cd8298da4a716ee0e123a015615028"
private const val SESSION_ID = "f50eb8326412b033276876e1f190110f21b81ff9"

interface TVApiService {

    @GET("3/tv/top_rated")
    fun getTopRated(@Query("api_key") apiKey: String = API_KEY): Deferred<TvShowsApiResponse>

    @GET("3/account/{account_id}/favorite/tv")
    fun getFavourites(
        @Query("session_id") sessionId: String = SESSION_ID,
        @Query("api_key") apiKey: String = API_KEY
    ): Deferred<TvShowsApiResponse>

    @GET("/3/tv/{id}")
    fun getShowDetails(
        @Path("id") id: Int,
        @Query("append_to_response") extras: String = "credits,recommendations,similar,account_states",
        @Query("session_id") sessionId: String = SESSION_ID,
        @Query("api_key") apiKey: String = API_KEY
    ): Deferred<Show>

    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("/3/tv/{id}/rating")
    fun submitRating(
        @Path("id") id: Int,
        @Body rate: TvRateRequestBody,
        @Query("session_id") sessionId: String = SESSION_ID,
        @Query("api_key") apiKey: String = API_KEY
    ): Deferred<SubmitRatingResponse>

    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("/3/account/{id}/favorite")
    fun submitFavourite(
        @Body fav: AddFavouriteRequestBody,
        @Query("session_id") sessionId: String = SESSION_ID,
        @Query("api_key") apiKey: String = API_KEY
    ): Deferred<SubmitFavouriteResponse>

}