package com.fadyemad.theair.network.moshiadapters

import com.fadyemad.theair.models.TvRate
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.json.JSONObject

class TvRateAdapter {
    /**
     * The backend return in the same Field two different object
     * like : rate : false
     *
     * or
     *       rate : {
     *              value : 2.5
     *              }
     */
    @FromJson fun fromJson (any: Any) : TvRate {
        if (any is Boolean){ // according to Backend Documentation this case will be no rate
            return TvRate(0.0f)
        }else {// has value
           val rate =  (any as Map<String, Double>)["value"]?.toFloat() ?: 0.0f
            return TvRate(rate)
        }
    }

    @ToJson fun toJson (tvRate: TvRate) : String {
        val jsonObject = JSONObject()
        jsonObject.put("value", tvRate.value.toDouble())
        return jsonObject.toString()
    }
}