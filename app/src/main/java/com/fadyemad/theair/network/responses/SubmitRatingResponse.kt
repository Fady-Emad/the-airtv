package com.fadyemad.theair.network.responses

import com.squareup.moshi.Json

data class SubmitRatingResponse(
    @Json(name = "success")
    val success: Boolean,
    @Json(name = "status_code")
    val status_code: Int,
    @Json(name = "status_message")
    val message: String
)