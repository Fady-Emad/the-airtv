package com.fadyemad.theair.network.requests

import com.squareup.moshi.Json

data class TvRateRequestBody (
    @Json(name = "value")
    val value : Float
)